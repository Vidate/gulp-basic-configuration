var gulp = require('gulp'),
    rimraf = require('rimraf'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    watch = require('gulp-watch'),
    tsc = require('gulp-tsc');

var paths = {
    src: './src/'
};

paths.ts = paths.src + 'modules/**/*.ts';
paths.js = paths.src + 'modules/**/*.js';
paths.jsFolder = paths.src + 'modules/';
paths.concatJsDest = paths.src + 'bin/site.min.js';

paths.less = paths.src + 'modules/**/*.less';
paths.concatCssDest = paths.src + 'bin/style.min.css';

gulp.task('clean-js', function (cb) {
    console.log('Clean up the jsmess');
    rimraf(paths.concatJsDest, cb);
});

gulp.task('clean-css', function (cb) {
    console.log('Clean up the cssmess');
    rimraf(paths.concatCssDest, cb);
});

gulp.task('compile-less', function () {
    console.log('Starting compile less!');
    return gulp.src(paths.less)
        .pipe(less().on('error', handleEvent))
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest('.'));
});

gulp.task('compile-ts', function () {
    return gulp.src(paths.ts)
        .pipe(tsc())
        .on('error', handleEvent)
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest('.'));
});

gulp.task("watch", function () {
    gulp.watch([paths.less], ["compile-less"]);
    gulp.watch([paths.ts], ["compile-ts"]);
});

gulp.task("clean", ["clean-js", "clean-css"]);

function handleEvent(error) {
    console.log(error.toString());
    console.log("error");
    this.emit('end');
}
